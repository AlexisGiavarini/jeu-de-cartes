package jeudecartes;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */

/**
 * Simulates a game
 */
public class Partie {
    protected JeuDeCartes jdc;
    protected ArrayList<Joueur> listeJoueurs;
    protected int nbCartesDistri;
    protected int score;

    /**
     * Create a game
     *
     * @param jdc the deck used for the game
     * @param lj the players
     * @param nbcartesTour
     * @param nbPointsGagnant
     */
    public Partie(ArrayList<Joueur> lj, JeuDeCartes jdc, int nbcartesTour, int nbPointsGagnant) {
        this.listeJoueurs = lj;
        this.jdc = jdc;
        this.nbCartesDistri = nbcartesTour;
        this.score = nbPointsGagnant;
    }

    /**
     * Gives a number of cards equal to nbCartesDistri every players 
     */
    public void distribuerCartes() {

        for (int i = 0; i < this.listeJoueurs.size(); i++) {
            this.listeJoueurs.get(i).recoitMain(new Main(this.jdc.donnerCartes(this.nbCartesDistri)));
        }
    }

    /**
     * Ranks the players according to their score
     * @return the position of every players
     */
    public ArrayList<Joueur> donneClassementTour() {
        ArrayList<Joueur> classementTour = this.listeJoueurs;
        Joueur tmp = null;
        
        for (int i = 0; i < classementTour.size(); i++) {
            for (int j = 0; j < classementTour.size(); j++) {
                if (classementTour.get(i).donneCredit() < classementTour.get(j).donneCredit()) {
                    tmp = classementTour.get(i);
                    classementTour.set(i, classementTour.get(j));
                    classementTour.set(j, tmp);
                }
            }
        }
        return classementTour;
    }

    /**
     * Returns the player with the highest score
     * @return the winner of the game
     */
    public Joueur donneGagnantPartie() {
        Joueur gagnantPartie = null;
        for (int i = 0; i < this.listeJoueurs.size(); i++) {
            if (this.listeJoueurs.get(i).donneCredit() == this.score) {
                gagnantPartie = this.listeJoueurs.get(i);
            }
        }
        return gagnantPartie;
    }

    /**
     * Returns the player with the hand this turn
     * @return the winner of the current turn
     */
    public Joueur donneGagnantTour() {
        Joueur gagnantTour = null;
        int bestHand = this.listeJoueurs.get(0).donneMain().donneValeur();
        gagnantTour = this.listeJoueurs.get(0);
        
        for (int i = 0; i < this.listeJoueurs.size(); i++) {
            if (this.listeJoueurs.get(i).donneMain().donneValeur() == bestHand && this.listeJoueurs.get(i) != this.listeJoueurs.get(0)) {
                gagnantTour = null;
            }
            if (this.listeJoueurs.get(i).donneMain().donneValeur() > bestHand) {
                bestHand = this.listeJoueurs.get(i).donneMain().donneValeur();
                gagnantTour = this.listeJoueurs.get(i);
            }
        }
        if(gagnantTour != null){
            gagnantTour.incrementeNbCredit();
        }
        return gagnantTour;
    }

    /**
     * Removes the players'cards and add them to the deck, then shuffles it
     */
    public void recupererCartes() {
        for (int i = 0; i < this.listeJoueurs.size(); i++) {
            this.jdc.cartes.addAll(0, this.listeJoueurs.get(i).donneMain().donneCartes());
        }
        
        for (int i = 0; i < this.listeJoueurs.size(); i++) {
            this.listeJoueurs.get(i).donneMain().donneCartes().clear();
        }
        
        this.jdc.melanger();
    }
}
