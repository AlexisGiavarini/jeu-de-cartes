/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeudecartes;

/**
 *
 * @author Alexis
 */

/**
 *
 * The rank of a card. Ace being the best value
 */
public enum Rang {
    AS, DEUX, TROIS, QUATRE, CINQ, SIX, SEPT, HUIT, NEUF, DIX, VALET, DAME, ROI
}
