package jeudecartes;

/**
 *
 * @author Alexis
 */

/**
 *
 * The color of a card
 */
public enum Couleur {
    CARREAU, PIQUE, COEUR, TREFLE
}
