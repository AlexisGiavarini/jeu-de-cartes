package jeudecartes;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */

/**
 * Creates a set of cards for the player
 *
 */
public class Main {
    protected ArrayList<Carte> cartes;
    
    /**
     * Creates a hand with the given cards
     * @param cartes the cards used to create the hand
     */
    public Main(ArrayList<Carte> cartes){
        this.cartes = new ArrayList<Carte>();
        this.cartes.addAll(cartes);
    }
    
    /**
     * Gives the cards in the player's hand
     * @return the cards used to create the hand
     */
    public ArrayList<Carte> donneCartes(){
        return this.cartes;
    }
    
    /**
     * Gives the total value of the cards in the player's hand
     * @return the total value of the cards
     */
    public int donneValeur(){
        int totalValue = 0;
        
        for (int i = 0; i < this.cartes.size(); i++) {
            totalValue += this.cartes.get(i).donneValeur();
        }      
        return totalValue;
    }
    
    public String toString(){
        return this.cartes.toString();
    }  
}
