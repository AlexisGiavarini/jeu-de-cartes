package jeudecartes;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JeuDeCartes jdc = new JeuDeCartes();
        jdc.melanger();
        ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
        joueurs.add(new Joueur("Fred"));
        joueurs.add(new Joueur("Paul"));
        joueurs.add(new Joueur("Tom"));
        Partie unePartie = new Partie(joueurs, jdc, 2, 10);
        Joueur gagnantTour = null;
        // tant qu'il n'y a pas de gagnant, on joue...
        while (unePartie.donneGagnantPartie() == null)
        {
          // un tour de jeu, distribution des cartes
          unePartie.distribuerCartes();
          // on determine le gagnant du tour
          gagnantTour = unePartie.donneGagnantTour();
          System.out.println("\n"+joueurs.get(0).nom+joueurs.get(0).donneMain().toString());
          System.out.println(joueurs.get(1).nom+joueurs.get(1).donneMain().toString());
          System.out.println(joueurs.get(2).nom+joueurs.get(2).donneMain().toString());
          if (gagnantTour != null)
          {
            System.out.println("Le gagnant du tour est " + gagnantTour);
          } else
          {
            System.out.println("Aucun gagnant pour le tour.");
          }
          // reinitialiser les conditions initiales
          unePartie.recupererCartes();
        }
        System.out.println(unePartie.donneGagnantPartie() + " remporte la partie...");
    }
}
