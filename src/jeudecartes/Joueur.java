package jeudecartes;

import java.util.ArrayList;

/**
 *
 * @author Alexis
 */

/**
 * Creates a player
 *
 */
public class Joueur {
    protected String nom;
    protected Main main;
    protected int credit;
    
    /**
     * Creates a player
     * @param nom the name of the player
     */
    public Joueur(String nom){
        this.nom = nom;
        this.credit = 0;
    }
    
    /**
     * Gives a player's current hand
     * @return the current player's hand
     */
    public Main donneMain(){
        return this.main;
    }
    
    /**
     * Gives the nmber of points a player has
     * @return the current player' score
     */
    public int donneCredit(){
        return this.credit;
    }
    
    /**
     * Increases the player'score
     */
    public void incrementeNbCredit(){
        this.credit++;
    }
    
    /**
     * Gives a player a number of cards
     */
    public void recoitMain(Main m){
        this.main = m;
    }
    
//    public String toString(){
//        return nom+" has "+"credit"+"\nCurrent hand is : "+main.toString();
//    }
    
    public String toString(){
        return nom;
    }
    
}
