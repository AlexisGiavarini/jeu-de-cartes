package jeudecartes;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Alexis
 */

/**
 * Creates a deck
 *
 */
public class JeuDeCartes {
    protected ArrayList<Carte> cartes;
    
    /**
     * Creates a sorted deck
     */
    public JeuDeCartes(){
        this.cartes = new ArrayList<Carte>();
        
        for (int i = 0; i < Couleur.values().length; i++) {
            for (int j = 0; j < Rang.values().length; j++) {
                this.cartes.add(new Carte(Rang.values()[j], Couleur.values()[i]));
            }
        }
    }
    
    
    /**
     * Shuffles the deck
     */
    public void melanger(){
        Collections.shuffle(this.cartes);    
    }
    
    /**
     * Gives a number of cards to a player, removing them from the deck
     * @param nb the number of cards given
     * @return the given cards
     */
    public ArrayList<Carte> donnerCartes(int nb){
        ArrayList<Carte> cartesDonnees = new ArrayList<Carte>();
        
        for (int i = 0; i < nb; i++) {
            cartesDonnees.add(this.cartes.get(0));
            this.cartes.remove(this.cartes.get(0));
        }
        
        return cartesDonnees;
    }
}
