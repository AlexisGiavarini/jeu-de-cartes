package jeudecartes;

/**
 *
 * @author Alexis
 */

/**
 * Creates a card (from a classic set)
 */
public class Carte {
    protected Couleur couleur;
    protected Rang rang;
    protected int valeur;

    /**
     * Creates a card with the specified rank and color
     * @param rg card rank
     * @param cl card color
     * 
     */
    public Carte(Rang rg, Couleur cl) {
        this.rang = rg;
        this.couleur = cl;
        
        for (int i = 0; i < Rang.values().length; i++) {
            if(Rang.values()[i] == rg){
                if(rg == Rang.AS){
                    this.valeur = 14; //l'as étant la carte la plus forte
                }else{
                   this.valeur = i+1; // +1 car on commence à 0
                }                
            }
        }
    }

    /**
     * Returns the color of the card
     * @return the card color
     * 
     */
    public Couleur donneCouleur() {
        return couleur;
    }

    /**
     * Returns the rank of the card
     * @return the card rank
     * 
     */
    public Rang donneRang() {
        return rang;
    }
    
    /**
     * Gives a value according to the rank
     * @return the card value
     * 
     */
    public int donneValeur(){
        return this.valeur;
    }
    
    public String toString(){
        return this.rang+" de "+this.couleur;
    }
}
